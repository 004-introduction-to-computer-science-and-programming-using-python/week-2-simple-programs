print("Please think of a number between 0 and 100!")
# create a variable to check if guess was correct
correct = False
topNumber = 100
bottomNumber = 0

while(correct == False):
    averageNumber = (topNumber + bottomNumber) // 2
    print("Is your secret number", averageNumber,"?")
# ask user for input
    guess = input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ")
# parse input
    if(guess == "h"):
        topNumber = averageNumber
        averageNumber = (topNumber + bottomNumber) // 2
    elif(guess == "l"):
        bottomNumber = averageNumber
        averageNumber = (topNumber + bottomNumber) // 2
    elif(guess == "c"):
        print("Game over. Your secret number was:", averageNumber)
        correct = True
    else:
        print("Sorry, I did not understand your input.")
