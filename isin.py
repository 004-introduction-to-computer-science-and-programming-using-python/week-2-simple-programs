def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string
    
    returns: True if char is in aStr; False otherwise
    '''
    # Your code here

    backupStr = aStr
    middleLetter = len(aStr) // 2
    bottom = False
    
    # if the middle character is the character then its good
    if(len(aStr) <= 1):
        return False

    # if theres nothing left we need to split it and see if its too big or too little

    # if theres something left and it hasn't returned true yet, return false
    elif(aStr[middleLetter] == char or aStr[0] == char or aStr[1] == char):
        return True
    
    # if theres stuff left we need to repeat the function until it returns the middle number
    elif(len(aStr) > 1):

        # happens if the character is after the middle letter
        if(char > aStr[middleLetter]):
            aStr = aStr[1:]
            return isIn(char, aStr)

        # happens if the character is before the middle letter
        elif(char < aStr[middleLetter]):
            topLetter = middleLetter
            aStr = aStr[:-1]
            return isIn(char, aStr)
        
        else:
            return False



print(isIn("n", ""))
