def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''
    result = 1
    while exp > 0:
        result *=base
        exp -= 1
    return result

print(iterPower(3.72, 3))

def recurPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0
 
    returns: int or float, base^exp
    '''
    # Your code here
    if(exp == 0):
        return 1
    elif(exp == 1):
        return base * 1
    return base * recurPower(base, exp - 1)

print(recurPower(3.72, 3))