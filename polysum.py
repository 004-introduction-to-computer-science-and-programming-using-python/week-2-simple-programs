import math

#   takes in variables n (number of sides) and s (side length)

def polysum(n, s):
    area = (.25 * n * s ** 2) / math.tan (math.pi / n)
    perimeter = s * n
    ans = area + perimeter ** 2
    return round(ans, 4) 
