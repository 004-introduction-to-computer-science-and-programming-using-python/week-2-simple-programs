def gcdIter(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here
    list = min(a, b)
    solved = False

    while(solved == False):
        if(b % list == 0 and a % list == 0):    
            return list
        else:
            list -= 1

print(gcdIter(161, 98))

def gcdRecur(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here
    if(b == 0):
        return a
    else:
        return gcdRecur(b, a % b)

print(gcdRecur(161, 98))