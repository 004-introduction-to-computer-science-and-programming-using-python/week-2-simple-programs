# # solution for pset 1
# balance = 484
# annualInterestRate = .2
# monthlyInterestRate =.04
# currentMonth = 0 

# while(currentMonth < 12):
#     minimumPayment = monthlyInterestRate * balance
#     monthlyUnpaidBalance = balance - minimumPayment
#     balance = round(monthlyUnpaidBalance + ( (annualInterestRate / 12) * monthlyUnpaidBalance ), 2)
#     currentMonth += 1
#     if(currentMonth == 12):
#         print("Remaining balance:", balance)

# # solution for pset 2

# balance = 3329
# resetBalance = balance
# annualInterestRate = .2
# totalFound = False
# fixedPayment = 0
# monthlyInterestRate = annualInterestRate / 12

# while(totalFound == False):
#     balance = resetBalance
#     for i in range(12):
#         minimumPayment = balance * monthlyInterestRate
#         monthlyUnpaidBalance = balance - fixedPayment
#         interest = monthlyUnpaidBalance * (annualInterestRate / 12)
#         balance = monthlyUnpaidBalance + interest
#     if(balance > 0):
#         fixedPayment += 10
#     else:
#         totalFound = True
# print("Lowest Payment: ",fixedPayment)

# working on pset 3

balance = 999999
resetBalance = balance
annualInterestRate = 0.18
monthlyPaymentRate = annualInterestRate / 12
epsilon = .02

topGuess = (balance * (( 1 + monthlyPaymentRate) ** 12)) / 12
bottomGuess = balance / 12
averageGuess = (topGuess + bottomGuess) / 2

while(abs(balance) >= epsilon):
    
    balance = resetBalance

    # calculate balance after 12 months
    for i in range(12):
        minimumPayment = balance * monthlyPaymentRate
        monthlyUnpaidBalance = balance - averageGuess
        interest = monthlyUnpaidBalance * (annualInterestRate / 12)
        balance = monthlyUnpaidBalance + interest

    # if the balance is positive above epsilon, its too little
    if(balance > epsilon):
        bottomGuess = averageGuess
        averageGuess = (topGuess + bottomGuess) / 2

    # if the balance is negative below negative epsilon its too high
    elif(balance < epsilon):
        topGuess = averageGuess
        averageGuess = (topGuess + bottomGuess) / 2
            
print("Lowest Payment: ",round(averageGuess, 2))

